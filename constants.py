month_quarter_map = {7: 'Quarter 1', 8: 'Quarter 1', 9: 'Quarter 1', 10: 'Quarter 2', 11: 'Quarter 2', 12: 'Quarter 2',
                        1: 'Quarter 3', 2: 'Quarter 3', 3: 'Quarter 3', 4: 'Quarter 4', 5: 'Quarter 4', 6: 'Quarter 4'}

month_map = {1: 'January', 2: 'February', 3: 'March', 4: 'April', 5: 'May', 6: 'June',
                        7: 'July', 8: 'August', 9: 'September', 10: 'October', 11: 'November', 12: 'December'}

fiscal_month_map = {7: 1, 8: 2, 9: 3, 10: 4, 11: 5, 12: 6,
                        1: 7, 2: 8, 3: 9, 4: 10, 5: 11, 6: 12}

scorecard_metrics_gen_dict = {'Auxiliary Consumption (%)':
                                {'alias': 'Auxiliary Consumption',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Availability Factor (%)':
                                {'alias': 'Availability Factor',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Average No. of Responsive bids':
                                {'alias': 'Average No. of Responsive bids',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Average time to procure - foreign spare parts (months)':
                                {'alias': 'Average time to procure - foreign spare parts (months)',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Capacity (MW)':
                                {'alias': 'Capacity (MW)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Employees per MW':
                                {'alias': 'Employees per MW',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Implementation of ADP (Financial) (%)':
                                {'alias': 'Implementation of Annual Development Program (Financial)(own financing, ECA & others)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Net Generation (GWh)':
                                {'alias': 'Net Generation (GWh)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Net Heat Rate (kJ/kWh)':
                                {'alias': 'Heat Rate(Net)',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'New capacity addition (MW)':
                                {'alias': 'New Capacity addition',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'OPEX excl. Fuel & Depreciation (Tk/MW)':
                                {'alias': 'OPEX excl. Fuel & Depreciation (Tk/MW)',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'OPEX (Tk/GWh)':
                                {'alias': 'OPEX (Tk/GWh)',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Percentage of Tenders Re-tendered (%)':
                                {'alias': 'Percentage of Tenders Re- tendered',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Plant Factor (%)':
                                {'alias': 'Plant Factor',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Power factor at HT side of step-up Transformer (%)':
                                {'alias': 'Power factor at HT side of step-up Transformer (%)',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Training Hour per Employee (Man-hour)':
                                {'alias': 'Average Annual Training hour',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                            }

scorecard_metrics_fin_dict = {'Quick ratio':
                                {'alias': 'Quick Ratio',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Current ratio':
                                {'alias': 'Current Ratio',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Debt service coverage ratio (DSCR)':
                                {'alias': 'Debt Service Coverage Ratio',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             }

scorecard_metrics_dsl_dict = {'DSL payment to the Government (Tk)':
                                {'alias': 'DSL payment to the Government',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                }
                             }

scorecard_metrics_dist_dict = {'OPEX excl. Fuel & Depreciation (Tk/km)':
                                {'alias': 'OPEX excl. Fuel & Depreciation (Tk/km)',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Losses (%)':
                                {'alias': 'Distribution system loss(at import level)',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Bill Collection Ratio (%)':
                                {'alias': 'Collection Bill Ratio (%)',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Reliability (SAIDI)':
                                {'alias': 'System Average Interruption Duration Index(SAIDI)',
                                 'reverse_kpi': True,
                                 'chunked': True,
                                },
                             'Construction of new distribution line (km)':
                                {'alias': 'Construction of distribution lines',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Construction/Capacity enhancement of distribution Sub-station (MVA)':
                                {'alias': 'Construction/ Capacity enhancement of distribution substation ',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Accounts Receivables (Eqv. months)':
                                {'alias': 'Accounts Receivable',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Reliability (SAIFI)':
                                {'alias': 'System Average Interruption Frequency Index(SAIFI)',
                                 'reverse_kpi': True,
                                 'chunked': True,
                                },
                             'Training Hour per Employee (Man-hour)':
                                {'alias': 'Average Annual Training hour',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Installation of Pre Payment Meter':
                                {'alias': 'Installation of prepaid meter',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'New Connection to the Households':
                                {'alias': 'New connection to the House Holds(HHs)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Accounts Payable (Months)':
                                {'alias': 'Accounts Payable',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Power Factor at each billing point (%)':
                                {'alias': 'Power Factor at each Billing Point',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Implementation of ADP (Financial) (%)':
                                {'alias': 'Implementation of Annual Development Program (Financial)  (own financing  & others)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Percentage of Overloaded Transformer (%)':
                                {'alias': 'Percentage of overloaded Transformer',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Distribution Lines (km)':
                                {'alias': 'Distribution Lines (km)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Capacity of 33/11 KV Sub-Station (MVA)':
                                {'alias': 'Capacity of 33/11 KV Sub-Station (MVA)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             '33/11 KV Sub-station (No)':
                                {'alias': '33/11 KV Sub-station (No)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Solar Charging Station Build and Operationalized (No.)':
                                {'alias': 'Solar Charging Station Build and Operationalized (No.)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'E-GP tendering (all local below 100 crore) which is applicable (%)':
                                {'alias': 'E-GP tendering (all local below 100 crore) which is applicable (%)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Bill distributed by Out Sources (No. of NOCS)':
                                {'alias': 'Bill distributed by Out Sources (No. of NOCS)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                            }

scorecard_metrics_trans_dict = {'Construction of transmission lines (CKm)':
                                {'alias': 'Construction of transmission lines',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Construction/Capacity enhancement of grid substation (MVA)':
                                {'alias': 'Construction/ Capacity enhancement of grid substation',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Transmission Loss (%)':
                                {'alias': 'Transmission Loss ',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'System Availability (%): Transmission Line':
                                {'alias': 'Transmission Line Availability',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Substation Availability (%)':
                                {'alias': 'Substation Availability',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'System Power Factor at each grid sub-station (%)':
                                {'alias': 'System Power Factor at each grid sub-station',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'System frequency sustained over 49.5 to 50.5 Hz in a year (%)':
                                {'alias': 'System frequency sustained between 49.6 to 50.4 Hz in a year',
                                 'reverse_kpi': False,
                                 'chunked': False,
                                },
                             'Training Hour per Employee (Man-hour)':
                                {'alias': 'Average Minimum Annual Training Hour',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                             'Percentage of Tenders Re-tendered (%)':
                                {'alias': 'Percentage of Tenders Re- Tendered',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Accounts Receivables (Eqv. months)':
                                {'alias': 'Accounts Receivable',
                                 'reverse_kpi': True,
                                 'chunked': False,
                                },
                             'Implementation of ADP (Financial) (%)':
                                {'alias': 'Implementation of Annual Development Program (Financial) (Financial) (own financing, ECA & others)',
                                 'reverse_kpi': False,
                                 'chunked': True,
                                },
                            }
