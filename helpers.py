import pandas as pd
import numpy as np
from datetime import datetime
from fuzzywuzzy import fuzz
import re
from constants import month_map, fiscal_month_map


def calc_year(fiscal_year, month_num):
    if month_num in range(1, 7):
        return fiscal_year[-4:]
    else:
        return fiscal_year[:4]


def apply_regex(input_str):
    output_str = re.sub(r'\W+', '', input_str).lower().strip()
    return output_str


def match_strings(data, string):
    matched_indices_list = []
    for (k1, v1) in data.items():
        for (k2, v2) in v1.items():
            if type(v2) == type(' ') and fuzz.token_set_ratio(v2, string) >= 95:
                matched_indices_tuple = (k2, k1)
                matched_indices_list.append(matched_indices_tuple)
    return matched_indices_list


def to_numeric(dataframe, column_name):
    if dataframe.dtypes[column_name] == 'O':
        dataframe[column_name] = dataframe[column_name].apply(lambda row: re.sub('[^0-9\.]','', str(row)))
        dataframe[column_name] = dataframe[column_name].apply(lambda row: pd.to_numeric(row, errors='coerce'))
    return dataframe[column_name]


def is_blank(value):
    if np.isnan(value):
        return True
    if value in [None, "", np.nan]:
        return True
    return False


def calc_variance_pct(actual, comparison):
    if is_blank(actual):
        return np.nan
    if comparison==0 or is_blank(comparison):
        return np.nan
    return round((actual - comparison) / abs(comparison), 2)


def calc_kpi_index(variance, reverse_kpi_bool):
    if is_blank(variance):
        return np.nan
    if reverse_kpi_bool:
        return 2 if variance > 0 else 1
    else:
        return 2 if variance < 0 else 1


def get_latest_measure(dataframe, measure_name, col_name):
    try:
        df_temp_latest = dataframe[dataframe['last_update_at']==max(dataframe['last_update_at'])]
        df_temp_measure_latest = df_temp_latest[df_temp_latest['measures']==measure_name]
        return df_temp_measure_latest[col_name].values[0]
    except Exception as e:
        print(e)
        return np.nan


def get_ytd_achievement(dataframe, measure_name):
    return get_latest_measure(dataframe, measure_name, 'ytd_achievement')


def calc_current_period_achievement(dataframe, measure_name, row, scorecard_metrics_dict):
    last_period_ytd_achievement = get_ytd_achievement(dataframe, measure_name)
    current_ytd_achievement = row['ytd_achievement']
    if last_period_ytd_achievement in [None, np.nan]:
        return current_ytd_achievement
    if scorecard_metrics_dict[measure_name]['chunked']:
        current_period_achievement = current_ytd_achievement - last_period_ytd_achievement
        return current_period_achievement
    else:
        return np.nan


def extract_data(scorecard_metrics_dict, file_metrics, df):
    scorecard_dict_partial = dict()
    scorecard_df_partial = pd.DataFrame()
    for file_metric in file_metrics:
        max_score = 0
        match_score = 0
        best_file_metric = None
        best_scorecard_metric = None
        for scorecard_metric_key, scorecard_metric_dict in scorecard_metrics_dict.items():
            match_score = fuzz.token_set_ratio(file_metric, scorecard_metric_dict['alias'])
            if match_score > max_score and match_score >= 95:
                max_score = match_score
                best_file_metric = file_metric
                best_scorecard_metric = scorecard_metric_key
        try:
            metric_loc = match_strings(df, best_file_metric)[0]
        except:
            continue

        scorecard_dict_partial['measures'] = best_scorecard_metric
        scorecard_dict_partial['annual_target'] = [df.iloc[metric_loc[0], metric_loc[1]+2]]
        scorecard_dict_partial['ytd_achievement'] = [df.iloc[metric_loc[0], metric_loc[1]+3]]
        df_temp = pd.DataFrame(scorecard_dict_partial)
        scorecard_df_partial = pd.concat([scorecard_df_partial, df_temp], axis=0)

    scorecard_df_partial.reset_index(drop=True, inplace=True)
    return scorecard_df_partial


def get_partial_processed_df(scorecard_metrics_dict, file_metrics, raw_df, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name, period='monthly'):
    scorecard_metrics_dict = scorecard_metrics_dict
    scorecard_df_partial = extract_data(scorecard_metrics_dict, file_metrics, raw_df)
    if period == 'monthly':
        scorecard_df_partial = post_process_df_monthly(scorecard_df_partial, scorecard_metrics_dict, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name)
    elif period == 'quarterly':
        scorecard_df_partial = post_process_df_quarterly(scorecard_df_partial, scorecard_metrics_dict, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name)
    else:
        scorecard_df_partial = post_process_df_annual(scorecard_df_partial, scorecard_metrics_dict, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name)
    return scorecard_df_partial


def process_ratio_data(df, colname):
    try:
        df[colname] = df.apply(lambda row: float(str(row[colname]).split(':')[0]) if ':' in row[colname] else row[colname], axis=1)
    except Exception as e:
        print(e)
    return df


def add_categoricals(df, date, year, fiscal_year, quarter, month_name, organization):
    datetime_now = datetime.now()
    df['last_update_at'] = datetime_now
    df['date'] = date
    df['year'] = year
    df['fiscal_year'] = fiscal_year
    df['quarter'] = quarter
    df['month_name'] = month_name
    df['organization'] = organization
    return df


def post_process_df_quarterly(scorecard_df_partial, scorecard_metrics_dict, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name):
    scorecard_df_partial['annual_target'] = to_numeric(scorecard_df_partial, 'annual_target')
    scorecard_df_partial['ytd_achievement'] = to_numeric(scorecard_df_partial, 'ytd_achievement')
    scorecard_df_partial = add_categoricals(scorecard_df_partial, date, year, fiscal_year, quarter, month_name, organization_name)
    scorecard_df_partial['ytd_target'] = scorecard_df_partial.apply(lambda row: row['annual_target']/12 * fiscal_month_map[datetime.strptime(row['month_name'], "%B").month] if scorecard_metrics_dict[row['measures']]['chunked'] else row['annual_target'], axis=1)
    scorecard_df_partial['latest_quarter_target'] = scorecard_df_partial.apply(lambda row: row['annual_target']/4 if scorecard_metrics_dict[row['measures']]['chunked'] else row['annual_target'], axis=1)
    scorecard_df_partial['ytd_target_variance'] = scorecard_df_partial.apply(lambda row: calc_variance_pct(row['ytd_achievement'], row['ytd_target']), axis=1)
    scorecard_df_partial['ytd_target_kpi_index'] = scorecard_df_partial.apply(lambda row: calc_kpi_index(row['ytd_target_variance'], scorecard_metrics_dict[row['measures']]['reverse_kpi']), axis=1)

    last_year_query = 'SELECT measures, ytd_achievement, last_update_at FROM {0} WHERE year = "{1}" AND quarter = "{2}" AND organization = "{3}"'.format(table_name, int(year)-1, quarter, organization_name)

    try:
        with engine.connect() as con:
            last_year_df_temp = pd.read_sql(sql=last_year_query, con=con)
        scorecard_df_partial['last_year_ytd'] = scorecard_df_partial.apply(lambda row: get_ytd_achievement(last_year_df_temp, row['measures']), axis=1)
    except:
        scorecard_df_partial['last_year_ytd'] = np.nan

    last_quarter_query = 'SELECT measures, ytd_achievement, last_update_at FROM {0} WHERE fiscal_year = "{1}" AND quarter = "Quarter {2}" AND organization = "{3}"'.format(table_name, fiscal_year, int(quarter.split()[-1])-1, organization_name)

    if scorecard_df_partial['quarter'].tolist()[0] == 'Quarter 1':
        scorecard_df_partial['latest_quarter_achievement'] = scorecard_df_partial['ytd_achievement']
    else:
        try:
            with engine.connect() as con:
                last_quarter_df_temp = pd.read_sql(sql=last_quarter_query, con=con)
                if last_quarter_df_temp.shape[0] == 0:
                    scorecard_df_partial['latest_quarter_achievement'] = np.nan
                else:
                    scorecard_df_partial['latest_quarter_achievement'] = scorecard_df_partial.apply(lambda row: calc_current_period_achievement(last_quarter_df_temp, row['measures'], row, scorecard_metrics_dict), axis=1)
        except:
            scorecard_df_partial['latest_quarter_achievement'] = np.nan

    # try:
    #     with engine.connect() as con:
    #         last_quarter_df_temp = pd.read_sql(sql=last_quarter_query, con=con)
    #     scorecard_df_partial['latest_quarter_achievement'] = scorecard_df_partial.apply(lambda row: calc_current_period_achievement(last_quarter_df_temp, row['measures'], row, scorecard_metrics_dict) if row['quarter'] != 'Quarter 1' else row['ytd_achievement'], axis=1)
    # except:
    #     scorecard_df_partial['latest_quarter_achievement'] = np.nan

    scorecard_df_partial['latest_quarter_target_variance'] = scorecard_df_partial.apply(lambda row: calc_variance_pct(row['latest_quarter_achievement'], row['latest_quarter_target']), axis=1)
    scorecard_df_partial['latest_quarter_target_kpi_index'] = scorecard_df_partial.apply(lambda row: calc_kpi_index(row['latest_quarter_target_variance'], scorecard_metrics_dict[row['measures']]['reverse_kpi']), axis=1)

    return scorecard_df_partial


def post_process_df_monthly(scorecard_df_partial, scorecard_metrics_dict, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name):
    scorecard_df_partial['annual_target'] = to_numeric(scorecard_df_partial, 'annual_target')
    scorecard_df_partial['ytd_achievement'] = to_numeric(scorecard_df_partial, 'ytd_achievement')
    scorecard_df_partial = add_categoricals(scorecard_df_partial, date, year, fiscal_year, quarter, month_name, organization_name)
    scorecard_df_partial['ytd_target'] = scorecard_df_partial.apply(lambda row: row['annual_target']/12 * fiscal_month_map[datetime.strptime(row['month_name'], "%B").month] if scorecard_metrics_dict[row['measures']]['chunked'] else row['annual_target'], axis=1)
    scorecard_df_partial['latest_month_target'] = scorecard_df_partial.apply(lambda row: row['annual_target']/12 if scorecard_metrics_dict[row['measures']]['chunked'] else row['annual_target'], axis=1)
    scorecard_df_partial['ytd_target_variance'] = scorecard_df_partial.apply(lambda row: calc_variance_pct(row['ytd_achievement'], row['ytd_target']), axis=1)
    scorecard_df_partial['ytd_target_kpi_index'] = scorecard_df_partial.apply(lambda row: calc_kpi_index(row['ytd_target_variance'], scorecard_metrics_dict[row['measures']]['reverse_kpi']), axis=1)

    last_year_query = 'SELECT measures, ytd_achievement, last_update_at FROM {0} WHERE year = "{1}" AND month_name = "{2}" AND organization = "{3}"'.format(table_name, int(year)-1, month_name, organization_name)

    try:
        with engine.connect() as con:
            last_year_df_temp = pd.read_sql(sql=last_year_query, con=con)
        scorecard_df_partial['last_year_ytd'] = scorecard_df_partial.apply(lambda row: get_ytd_achievement(last_year_df_temp, row['measures']), axis=1)
    except:
        scorecard_df_partial['last_year_ytd'] = np.nan
    try:
        prev_month = month_map[datetime.strptime(month_name, "%B").month-1]
    except:
        prev_month = 'December'
    last_month_query = 'SELECT measures, ytd_achievement, last_update_at FROM {0} WHERE fiscal_year = "{1}" AND month_name = "{2}" AND organization = "{3}"'.format(table_name, fiscal_year, prev_month, organization_name)
    if scorecard_df_partial['month_name'].tolist()[0] == 'July':
        scorecard_df_partial['latest_month_achievement'] = scorecard_df_partial['ytd_achievement']
    else:
        try:
            with engine.connect() as con:
                last_month_df_temp = pd.read_sql(sql=last_month_query, con=con)
                if last_month_df_temp.shape[0] == 0:
                    scorecard_df_partial['latest_month_achievement'] = np.nan
                else:
                    scorecard_df_partial['latest_month_achievement'] = scorecard_df_partial.apply(lambda row: calc_current_period_achievement(last_month_df_temp, row['measures'], row, scorecard_metrics_dict), axis=1)
        except:
            scorecard_df_partial['latest_month_achievement'] = np.nan

    # try:
    #     with engine.connect() as con:
    #         last_month_df_temp = pd.read_sql(sql=last_month_query, con=con)
    # except:
    #     scorecard_df_partial['latest_month_achievement'] = np.nan
    # scorecard_df_partial['latest_month_achievement'] = scorecard_df_partial.apply(lambda row: calc_current_period_achievement(last_month_df_temp, row['measures'], row, scorecard_metrics_dict) if row['month_name'] != 'July' else row['ytd_achievement'], axis=1)

    scorecard_df_partial['latest_month_target_variance'] = scorecard_df_partial.apply(lambda row: calc_variance_pct(row['latest_month_achievement'], row['latest_month_target']), axis=1)
    scorecard_df_partial['latest_month_target_kpi_index'] = scorecard_df_partial.apply(lambda row: calc_kpi_index(row['latest_month_target_variance'], scorecard_metrics_dict[row['measures']]['reverse_kpi']), axis=1)

    return scorecard_df_partial


def post_process_df_annual(scorecard_df_partial, scorecard_metrics_dict, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name):
    scorecard_df_partial['annual_target'] = to_numeric(scorecard_df_partial, 'annual_target')
    scorecard_df_partial['ytd_achievement'] = to_numeric(scorecard_df_partial, 'ytd_achievement')
    scorecard_df_partial = add_categoricals(scorecard_df_partial, date, year, fiscal_year, quarter, month_name, organization_name)
    # scorecard_df_partial['ytd_target'] = scorecard_df_partial.apply(lambda row: row['annual_target']/12 * int(row['quarter'].split()[-1]) if scorecard_metrics_dict[row['measures']]['chunked'] else row['annual_target'], axis=1)
    # scorecard_df_partial['latest_quarter_target'] = scorecard_df_partial.apply(lambda row: row['annual_target']/12 * datetime.strptime(row['month_name'], "%B").month if scorecard_metrics_dict[row['measures']]['chunked'] else row['annual_target'], axis=1)
    scorecard_df_partial['ytd_target_variance'] = scorecard_df_partial.apply(lambda row: calc_variance_pct(row['ytd_achievement'], row['annual_target']), axis=1)
    scorecard_df_partial['ytd_target_kpi_index'] = scorecard_df_partial.apply(lambda row: calc_kpi_index(row['ytd_target_variance'], scorecard_metrics_dict[row['measures']]['reverse_kpi']), axis=1)

    last_year_query = 'SELECT measures, ytd_achievement, last_update_at FROM {0} WHERE year = "{1}" AND month_name = "{2}" AND organization = "{3}"'.format(table_name, int(year)-1, month_name, organization_name)

    try:
        with engine.connect() as con:
            last_year_df_temp = pd.read_sql(sql=last_year_query, con=con)
        scorecard_df_partial['last_year_ytd'] = scorecard_df_partial.apply(lambda row: get_ytd_achievement(last_year_df_temp, row['measures']), axis=1)
    except:
        scorecard_df_partial['last_year_ytd'] = np.nan

    # last_quarter_query = 'SELECT measures, ytd_achievement, last_update_at FROM {0} WHERE fiscal_year = "{1}" AND quarter = "Quarter {2}" AND organization = "{3}"'.format(table_name, fiscal_year, int(quarter.split()[-1])-1, organization_name)
    #
    # try:
    #     with engine.connect() as con:
    #         last_quarter_df_temp = pd.read_sql(sql=last_quarter_query, con=con)
    #     scorecard_df_partial['latest_quarter_achievement'] = scorecard_df_partial.apply(lambda row: calc_current_quarter_achievement(last_quarter_df_temp, row['measures'], row, scorecard_metrics_dict) if row['quarter'] != 'Quarter 1' else row['ytd_achievement'], axis=1)
    # except:
    #     scorecard_df_partial['latest_quarter_achievement'] = np.nan

    scorecard_df_partial['yoy_variance'] = scorecard_df_partial.apply(lambda row: calc_variance_pct(row['ytd_achievement'], row['last_year_ytd']), axis=1)
    scorecard_df_partial['yoy_kpi_index'] = scorecard_df_partial.apply(lambda row: calc_kpi_index(row['yoy_variance'], scorecard_metrics_dict[row['measures']]['reverse_kpi']), axis=1)

    return scorecard_df_partial
