import time
import pandas as pd
from helpers import *
from constants import *
from secrets import CONN_STRING
from sqlalchemy import create_engine
from datetime import datetime


pd.set_option('display.max_columns', 20)
filepath='C:\\Users\\jawad\\dev\\bcg_power_data_local\\data files for fileupload demo\\generation scorecard data template monthly.xls'
organization = 'EGCB'
fiscal_year = '2018-2019'
month_num = 6


def insert_file_data_to_db(filepath, organization, fiscal_year, month_num):
    df = pd.DataFrame()
    try:
        if filepath.endswith('.csv'):
            df = pd.read_csv(filepath, header=None, thousands=',')
        else:
            df = pd.read_excel(filepath, header=None, index=None, thousands=',')
        df = df[df[1].notnull()].reset_index(drop=True)
    except Exception as e:
        print(e)
    engine = create_engine(CONN_STRING)

    year = calc_year(fiscal_year, month_num)
    date = datetime.strptime('1/{0}/{1}'.format(month_num, year), '%d/%m/%Y')
    month_name = date.strftime("%B")
    quarter = month_quarter_map[month_num]
    metrics_loc = match_strings(df, 'Performance')[0]

    file_metrics = df.iloc[metrics_loc[0]+1:,metrics_loc[1]].values.tolist()
    if 'Total' in file_metrics:
        file_metrics.remove('Total')

    if organization in ['BPDB (Generation)', 'BPDB (Distribution)']:
        organization_name = 'BPDB'
    else:
        organization_name = organization

    if organization in ['BPDB (Generation)', 'APSCL', 'EGCB', 'NWPGCL']:
        table_name = 'scorecard_generation_monthly'
        scorecard_df_partial = get_partial_processed_df(scorecard_metrics_gen_dict, file_metrics, df, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name)
        with engine.connect() as con:
            scorecard_df_partial.to_sql(name=table_name, con=con, if_exists='append', index=False)
        if month_name in ['September', 'December', 'March', 'June']:
            table_name = 'scorecard_generation_quarterly'
            scorecard_df_partial = get_partial_processed_df(scorecard_metrics_gen_dict, file_metrics, df, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name, 'quarterly')
            with engine.connect() as con:
                scorecard_df_partial.to_sql(name=table_name, con=con, if_exists='append', index=False)
        if month_name in ['June']:
            table_name = 'scorecard_generation_annual'
            scorecard_df_partial = get_partial_processed_df(scorecard_metrics_gen_dict, file_metrics, df, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name, 'annual')
            with engine.connect() as con:
                scorecard_df_partial.to_sql(name=table_name, con=con, if_exists='append', index=False)

    elif organization in ['BPDB (Distribution)', 'WZPDCL', 'DESCO', 'DPDC']:
        table_name = 'scorecard_distribution_monthly'
        scorecard_df_partial = get_partial_processed_df(scorecard_metrics_dist_dict, file_metrics, df, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name)
        with engine.connect() as con:
            scorecard_df_partial.to_sql(name=table_name, con=con, if_exists='append', index=False)
        if month_name in ['September', 'December', 'March', 'June']:
            table_name = 'scorecard_distribution_quarterly'
            scorecard_df_partial = get_partial_processed_df(scorecard_metrics_gen_dict, file_metrics, df, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name, 'quarterly')
            with engine.connect() as con:
                scorecard_df_partial.to_sql(name=table_name, con=con, if_exists='append', index=False)
        if month_name in ['June']:
            table_name = 'scorecard_distribution_annual'
            scorecard_df_partial = get_partial_processed_df(scorecard_metrics_gen_dict, file_metrics, df, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name, 'annual')
            with engine.connect() as con:
                scorecard_df_partial.to_sql(name=table_name, con=con, if_exists='append', index=False)

    elif organization in ['PGCB']:
        table_name = 'scorecard_transmission_monthly'
        scorecard_df_partial = get_partial_processed_df(scorecard_metrics_trans_dict, file_metrics, df, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name)
        with engine.connect() as con:
            scorecard_df_partial.to_sql(name=table_name, con=con, if_exists='append', index=False)
        if month_name in ['September', 'December', 'March', 'June']:
            table_name = 'scorecard_transmission_quarterly'
            scorecard_df_partial = get_partial_processed_df(scorecard_metrics_gen_dict, file_metrics, df, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name, 'quarterly')
            with engine.connect() as con:
                scorecard_df_partial.to_sql(name=table_name, con=con, if_exists='append', index=False)
        if month_name in ['June']:
            table_name = 'scorecard_transmission_annual'
            scorecard_df_partial = get_partial_processed_df(scorecard_metrics_gen_dict, file_metrics, df, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name, 'annual')
            with engine.connect() as con:
                scorecard_df_partial.to_sql(name=table_name, con=con, if_exists='append', index=False)

    table_name_financial = 'scorecard_financial_monthly'
    scorecard_df_partial_fin = extract_data(scorecard_metrics_fin_dict, file_metrics, df)
    scorecard_df_partial_fin = process_ratio_data(scorecard_df_partial_fin, 'annual_target')
    scorecard_df_partial_fin = process_ratio_data(scorecard_df_partial_fin, 'ytd_achievement')
    scorecard_df_partial_fin_monthly = scorecard_df_partial_fin.copy()
    scorecard_df_partial_fin_monthly = post_process_df_monthly(scorecard_df_partial_fin_monthly, scorecard_metrics_fin_dict, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name_financial)
    with engine.connect() as con:
        scorecard_df_partial_fin_monthly.to_sql(name=table_name_financial, con=con, if_exists='append', index=False)
    if month_name in ['September', 'December', 'March', 'June']:
        table_name_financial = 'scorecard_financial_quarterly'
        scorecard_df_partial_fin_quarterly = scorecard_df_partial_fin.copy()
        scorecard_df_partial_fin_quarterly = post_process_df_quarterly(scorecard_df_partial_fin_quarterly, scorecard_metrics_fin_dict, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name_financial)
        with engine.connect() as con:
            scorecard_df_partial_fin_quarterly.to_sql(name=table_name_financial, con=con, if_exists='append', index=False)
    if month_name in ['June']:
        table_name_financial = 'scorecard_financial_annual'
        scorecard_df_partial_fin_annual = scorecard_df_partial_fin.copy()
        scorecard_df_partial_fin_annual = post_process_df_annual(scorecard_df_partial_fin_annual, scorecard_metrics_fin_dict, engine, date, year, fiscal_year, quarter, month_name, organization_name, table_name_financial)
        with engine.connect() as con:
            scorecard_df_partial_fin_annual.to_sql(name=table_name_financial, con=con, if_exists='append', index=False)

    table_name_dsl = 'scorecard_dsl_monthly'
    scorecard_df_partial_dsl = extract_data(scorecard_metrics_dsl_dict, file_metrics, df)
    scorecard_df_partial_dsl = add_categoricals(scorecard_df_partial_dsl, date, year, fiscal_year, quarter, month_name, organization_name)
    with engine.connect() as con:
        scorecard_df_partial_dsl.to_sql(name=table_name_dsl, con=con, if_exists='append', index=False)
    if month_name in ['September', 'December', 'March', 'June']:
        table_name_dsl = 'scorecard_dsl_quarterly'
        scorecard_df_partial_dsl = extract_data(scorecard_metrics_dsl_dict, file_metrics, df)
        scorecard_df_partial_dsl = add_categoricals(scorecard_df_partial_dsl, date, year, fiscal_year, quarter, month_name, organization_name)
        with engine.connect() as con:
            scorecard_df_partial_dsl.to_sql(name=table_name_dsl, con=con, if_exists='append', index=False)
    if month_name in ['June']:
        table_name_dsl = 'scorecard_dsl_annual'
        scorecard_df_partial_dsl = extract_data(scorecard_metrics_dsl_dict, file_metrics, df)
        scorecard_df_partial_dsl = add_categoricals(scorecard_df_partial_dsl, date, year, fiscal_year, quarter, month_name, organization_name)
        with engine.connect() as con:
            scorecard_df_partial_dsl.to_sql(name=table_name_dsl, con=con, if_exists='append', index=False)

insert_file_data_to_db(filepath, organization, fiscal_year, month_num)
